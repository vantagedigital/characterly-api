<?php

/* API Interface */

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', array('namespace' => 'Characterly\\Controllers'), function (Dingo\Api\Routing\Router $api) {

    foreach (glob(__DIR__ . '/api/*.php') as $routeFile) {
        require $routeFile;
    }

});