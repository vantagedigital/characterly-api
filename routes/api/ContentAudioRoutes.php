<?php

$api->group(array('prefix' => 'content-audio', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentAudioController@makeup');
    $api->get('search', 'ContentAudioController@search');

    $api->post('', 'ContentAudioController@store');

    $api->group(array('prefix' => '{contentAudioId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentAudioController@delete');

        $api->get('', 'ContentAudioController@show');

        $api->put('', 'ContentAudioController@update');

    });

});