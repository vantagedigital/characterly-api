<?php

$api->group(array('prefix' => 'content-approval', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentApprovalController@makeup');
    $api->get('search', 'ContentApprovalController@search');

    $api->post('', 'ContentApprovalController@store');

    $api->group(array('prefix' => '{contentApprovalId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentApprovalController@delete');

        $api->get('', 'ContentApprovalController@show');

        $api->put('', 'ContentApprovalController@update');

    });

});