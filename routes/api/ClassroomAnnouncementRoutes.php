<?php

$api->group(array('prefix' => 'classroom-announcement', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ClassroomAnnouncementController@makeup');
    $api->get('search', 'ClassroomAnnouncementController@search');

    $api->post('', 'ClassroomAnnouncementController@store');

    $api->group(array('prefix' => '{classroomAnnouncementId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ClassroomAnnouncementController@delete');

        $api->get('', 'ClassroomAnnouncementController@show');

        $api->put('', 'ClassroomAnnouncementController@update');

    });

});