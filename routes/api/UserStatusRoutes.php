<?php

$api->group(array('prefix' => 'user-status', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'UserStatusController@makeup');
    $api->get('search', 'UserStatusController@search');

    $api->post('', 'UserStatusController@store');

    $api->group(array('prefix' => '{userStatusId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'UserStatusController@delete');

        $api->get('', 'UserStatusController@show');

        $api->put('', 'UserStatusController@update');

    });

});