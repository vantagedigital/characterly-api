<?php

$api->group(array('prefix' => 'user-role', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'UserRoleController@makeup');
    $api->get('search', 'UserRoleController@search');

    $api->post('', 'UserRoleController@store');

    $api->group(array('prefix' => '{userRoleId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'UserRoleController@delete');

        $api->get('', 'UserRoleController@show');

        $api->put('', 'UserRoleController@update');

    });

});