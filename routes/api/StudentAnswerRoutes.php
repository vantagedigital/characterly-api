<?php

$api->group(array('prefix' => 'student-answer', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'StudentAnswerController@makeup');
    $api->get('search', 'StudentAnswerController@search');

    $api->post('', 'StudentAnswerController@store');

    $api->group(array('prefix' => '{studentAnswerId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'StudentAnswerController@delete');

        $api->get('', 'StudentAnswerController@show');

        $api->put('', 'StudentAnswerController@update');

    });

});