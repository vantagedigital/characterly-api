<?php

$api->group(array('prefix' => 'evaluation-question', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'EvaluationQuestionController@makeup');
    $api->get('search', 'EvaluationQuestionController@search');

    $api->post('', 'EvaluationQuestionController@store');

    $api->group(array('prefix' => '{evaluationQuestionId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'EvaluationQuestionController@delete');

        $api->get('', 'EvaluationQuestionController@show');

        $api->put('', 'EvaluationQuestionController@update');

    });

});