<?php

$api->group(array('prefix' => 'evaluation-question-type', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'EvaluationQuestionTypeController@makeup');
    $api->get('search', 'EvaluationQuestionTypeController@search');

    $api->post('', 'EvaluationQuestionTypeController@store');

    $api->group(array('prefix' => '{evaluationQuestionTypeId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'EvaluationQuestionTypeController@delete');

        $api->get('', 'EvaluationQuestionTypeController@show');

        $api->put('', 'EvaluationQuestionTypeController@update');

    });

});