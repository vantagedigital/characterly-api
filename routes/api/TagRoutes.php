<?php

$api->group(array('prefix' => 'tag', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'TagController@makeup');
    $api->get('search', 'TagController@search');

    $api->post('', 'TagController@store');

    $api->group(array('prefix' => '{tagId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'TagController@delete');

        $api->get('', 'TagController@show');

        $api->put('', 'TagController@update');

    });

});