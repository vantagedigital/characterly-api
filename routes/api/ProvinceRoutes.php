<?php

$api->group(array('prefix' => 'province', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ProvinceController@makeup');
    $api->get('search', 'ProvinceController@search');

    $api->post('', 'ProvinceController@store');

    $api->group(array('prefix' => '{provinceId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ProvinceController@delete');

        $api->get('', 'ProvinceController@show');

        $api->put('', 'ProvinceController@update');

    });

});