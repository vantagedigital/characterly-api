<?php

$api->group(array('prefix' => 'country', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'CountryController@makeup');
    $api->get('search', 'CountryController@search');

    $api->post('', 'CountryController@store');

    $api->group(array('prefix' => '{countryId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'CountryController@delete');

        $api->get('', 'CountryController@show');

        $api->put('', 'CountryController@update');

    });

});