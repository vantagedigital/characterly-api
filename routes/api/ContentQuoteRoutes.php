<?php

$api->group(array('prefix' => 'content-quote', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentQuoteController@makeup');
    $api->get('search', 'ContentQuoteController@search');

    $api->post('', 'ContentQuoteController@store');

    $api->group(array('prefix' => '{contentQuoteId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentQuoteController@delete');

        $api->get('', 'ContentQuoteController@show');

        $api->put('', 'ContentQuoteController@update');

    });

});