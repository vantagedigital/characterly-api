<?php

$api->group(array('prefix' => 'user', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'UserController@makeup');
    $api->get('search', 'UserController@search');

    $api->post('', 'UserController@store');

    $api->group(array('prefix' => '{userId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'UserController@delete');

        $api->get('', 'UserController@show');

        $api->put('', 'UserController@update');

    });

});