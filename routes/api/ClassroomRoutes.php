<?php

$api->group(array('prefix' => 'classroom', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ClassroomController@makeup');
    $api->get('search', 'ClassroomController@search');

    $api->post('', 'ClassroomController@store');

    $api->group(array('prefix' => '{classroomId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ClassroomController@delete');

        $api->get('', 'ClassroomController@show');

        $api->put('', 'ClassroomController@update');

    });

});