<?php

$api->group(array('prefix' => 'pillar', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'PillarController@makeup');
    $api->get('search', 'PillarController@search');

    $api->post('', 'PillarController@store');

    $api->group(array('prefix' => '{pillarId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'PillarController@delete');

        $api->get('', 'PillarController@show');

        $api->put('', 'PillarController@update');

    });

});