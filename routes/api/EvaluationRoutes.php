<?php

$api->group(array('prefix' => 'evaluation', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'EvaluationController@makeup');
    $api->get('search', 'EvaluationController@search');

    $api->post('', 'EvaluationController@store');

    $api->group(array('prefix' => '{evaluationId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'EvaluationController@delete');

        $api->get('', 'EvaluationController@show');

        $api->put('', 'EvaluationController@update');

    });

});