<?php

$api->group(array('prefix' => 'acl-activity-log', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'AclActivityLogController@makeup');
    $api->get('search', 'AclActivityLogController@search');

    $api->post('', 'AclActivityLogController@store');

    $api->group(array('prefix' => '{aclActivityLogId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'AclActivityLogController@delete');

        $api->get('', 'AclActivityLogController@show');

        $api->put('', 'AclActivityLogController@update');

    });

});