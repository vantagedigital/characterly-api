<?php

$api->group(array('prefix' => 'acl-permission', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'AclPermissionController@makeup');
    $api->get('search', 'AclPermissionController@search');

    $api->post('', 'AclPermissionController@store');

    $api->group(array('prefix' => '{aclPermissionId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'AclPermissionController@delete');

        $api->get('', 'AclPermissionController@show');

        $api->put('', 'AclPermissionController@update');

    });

});