<?php

$api->group(array('prefix' => 'content-video', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentVideoController@makeup');
    $api->get('search', 'ContentVideoController@search');

    $api->post('', 'ContentVideoController@store');

    $api->group(array('prefix' => '{contentVideoId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentVideoController@delete');

        $api->get('', 'ContentVideoController@show');

        $api->put('', 'ContentVideoController@update');

    });

});