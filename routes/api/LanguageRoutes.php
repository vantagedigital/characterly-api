<?php

$api->group(array('prefix' => 'language', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'LanguageController@makeup');
    $api->get('search', 'LanguageController@search');

    $api->post('', 'LanguageController@store');

    $api->group(array('prefix' => '{languageId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'LanguageController@delete');

        $api->get('', 'LanguageController@show');

        $api->put('', 'LanguageController@update');

    });

});