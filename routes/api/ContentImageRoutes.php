<?php

$api->group(array('prefix' => 'content-image', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentImageController@makeup');
    $api->get('search', 'ContentImageController@search');

    $api->post('', 'ContentImageController@store');

    $api->group(array('prefix' => '{contentImageId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentImageController@delete');

        $api->get('', 'ContentImageController@show');

        $api->put('', 'ContentImageController@update');

    });

});