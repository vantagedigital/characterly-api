<?php

$api->group(array('prefix' => 'auth'), function (Dingo\Api\Routing\Router $api) {

    $api->post('debug-login/{$id}', 'AuthController@debugLogin');
    $api->post('login', 'AuthController@login');
    $api->post('logout', 'AuthController@logout');
    $api->post('register', 'AuthController@register');

    $api->group(array('middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

        $api->get('authenticated-user', 'AuthController@getAuthenticatedUser');

    });

    $api->group(array('middleware' => 'auth.user'), function (Dingo\Api\Routing\Router $api) {

        $api->post('logout', 'AuthController@logout');

    });

});