<?php

$api->group(array('prefix' => 'content', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentController@makeup');
    $api->get('search', 'ContentController@search');

    $api->post('', 'ContentController@store');

    $api->group(array('prefix' => '{contentId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentController@delete');

        $api->get('', 'ContentController@show');

        $api->put('', 'ContentController@update');

    });

});