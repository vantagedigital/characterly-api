<?php

$api->group(array('prefix' => 'acl-resource', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'AclResourceController@makeup');
    $api->get('search', 'AclResourceController@search');

    $api->post('', 'AclResourceController@store');

    $api->group(array('prefix' => '{aclResourceId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'AclResourceController@delete');

        $api->get('', 'AclResourceController@show');

        $api->put('', 'AclResourceController@update');

    });

});