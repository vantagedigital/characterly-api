<?php

$api->group(array('prefix' => 'city', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'CityController@makeup');
    $api->get('search', 'CityController@search');

    $api->post('', 'CityController@store');

    $api->group(array('prefix' => '{cityId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'CityController@delete');

        $api->get('', 'CityController@show');

        $api->put('', 'CityController@update');

    });

});