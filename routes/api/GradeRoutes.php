<?php

$api->group(array('prefix' => 'grade', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'GradeController@makeup');
    $api->get('search', 'GradeController@search');

    $api->post('', 'GradeController@store');

    $api->group(array('prefix' => '{gradeId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'GradeController@delete');

        $api->get('', 'GradeController@show');

        $api->put('', 'GradeController@update');

    });

});