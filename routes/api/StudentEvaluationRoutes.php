<?php

$api->group(array('prefix' => 'student-evaluation', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'StudentEvaluationController@makeup');
    $api->get('search', 'StudentEvaluationController@search');

    $api->post('', 'StudentEvaluationController@store');

    $api->group(array('prefix' => '{studentEvaluationId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'StudentEvaluationController@delete');

        $api->get('', 'StudentEvaluationController@show');

        $api->put('', 'StudentEvaluationController@update');

    });

});