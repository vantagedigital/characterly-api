<?php

$api->group(array('prefix' => 'content-article', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentArticleController@makeup');
    $api->get('search', 'ContentArticleController@search');

    $api->post('', 'ContentArticleController@store');

    $api->group(array('prefix' => '{contentArticleId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentArticleController@delete');

        $api->get('', 'ContentArticleController@show');

        $api->put('', 'ContentArticleController@update');

    });

});