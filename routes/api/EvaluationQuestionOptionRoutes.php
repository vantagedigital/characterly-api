<?php

$api->group(array('prefix' => 'evaluation-question-option', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'EvaluationQuestionOptionController@makeup');
    $api->get('search', 'EvaluationQuestionOptionController@search');

    $api->post('', 'EvaluationQuestionOptionController@store');

    $api->group(array('prefix' => '{evaluationQuestionOptionId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'EvaluationQuestionOptionController@delete');

        $api->get('', 'EvaluationQuestionOptionController@show');

        $api->put('', 'EvaluationQuestionOptionController@update');

    });

});