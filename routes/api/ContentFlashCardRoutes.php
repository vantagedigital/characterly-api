<?php

$api->group(array('prefix' => 'content-flash-card', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'ContentFlashCardController@makeup');
    $api->get('search', 'ContentFlashCardController@search');

    $api->post('', 'ContentFlashCardController@store');

    $api->group(array('prefix' => '{contentFlashCardId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'ContentFlashCardController@delete');

        $api->get('', 'ContentFlashCardController@show');

        $api->put('', 'ContentFlashCardController@update');

    });

});