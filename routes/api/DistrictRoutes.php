<?php

$api->group(array('prefix' => 'district', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'DistrictController@makeup');
    $api->get('search', 'DistrictController@search');

    $api->post('', 'DistrictController@store');

    $api->group(array('prefix' => '{districtId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'DistrictController@delete');

        $api->get('', 'DistrictController@show');

        $api->put('', 'DistrictController@update');

    });

});