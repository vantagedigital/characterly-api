<?php

$api->group(array('prefix' => 'evaluation-type', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'EvaluationTypeController@makeup');
    $api->get('search', 'EvaluationTypeController@search');

    $api->post('', 'EvaluationTypeController@store');

    $api->group(array('prefix' => '{evaluationTypeId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'EvaluationTypeController@delete');

        $api->get('', 'EvaluationTypeController@show');

        $api->put('', 'EvaluationTypeController@update');

    });

});