<?php

$api->group(array('prefix' => 'school', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', 'SchoolController@makeup');
    $api->get('search', 'SchoolController@search');

    $api->post('', 'SchoolController@store');

    $api->group(array('prefix' => '{schoolId}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', 'SchoolController@delete');

        $api->get('', 'SchoolController@show');

        $api->put('', 'SchoolController@update');

    });

});