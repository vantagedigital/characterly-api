<?php

$api->group(array('prefix' => '[Entity-0]', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('makeup/{type}', '[Entity0]Controller@makeup');
    $api->get('search', '[Entity0]Controller@search');

    $api->post('', '[Entity0]Controller@store');

    $api->group(array('prefix' => '{[entity0]Id}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', '[Entity0]Controller@delete');

        $api->get('', '[Entity0]Controller@show');

        $api->put('', '[Entity0]Controller@update');

    });

});