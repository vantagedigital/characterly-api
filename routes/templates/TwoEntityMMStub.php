<?php

$api->group(array('prefix' => '[Entity-0]/{[entity0]Id}/[Entity-1]', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->get('search', '[Entity0][Entity1]Controller@search');

    $api->group(array('prefix' => '{[entity1]Id}'), function (Dingo\Api\Routing\Router $api) {

        $api->delete('', '[Entity0][Entity1]Controller@delete');

        $api->get('', '[Entity0][Entity1]Controller@show');

        $api->post('', '[Entity0][Entity1]Controller@store');

        $api->put('', '[Entity0][Entity1]Controller@update');

    });

});