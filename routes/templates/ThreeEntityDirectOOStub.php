<?php

$api->group(array('prefix' => '[Entity-0]/{[entity0]Id}/[Entity-1]/{[entity1]Id}/[Entity-2]', 'middleware' => 'api'), function (Dingo\Api\Routing\Router $api) {

    $api->delete('', '[Entity0][Entity1][Entity2]Controller@delete');

    $api->get('', '[Entity0][Entity1][Entity2]Controller@show');

    $api->post('', '[Entity0][Entity1][Entity2]Controller@store');

    $api->put('', '[Entity0][Entity1][Entity2]Controller@update');

});