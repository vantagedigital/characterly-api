<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('user_id');
            $table->content('description');
            $table->string('motto')->index();
            $table->unsignedInteger('reward_points');
            $table->dateTime('due_date');
            $table->timestamps();
        });
        Schema::create('challenges_pillars', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'challenge_id', 'pillar_id');
        });
        Schema::create('challenges_students', function (Blueprint $blueprint) {

            $table = TableBuilder::createLinkTable($blueprint, 'challenge_id');
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->timestamps();
        });
        Schema::create('student_challenge_attempts', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->foreignKey('challenge_id');
            $table->boolean('is_complete')->default(false);
            $table->unsignedInteger('points_earned')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
        Schema::dropIfExists('challenge_pillars');
        Schema::dropIfExists('challenges_students');
        Schema::dropIfExists('student_challenges');
    }
}
