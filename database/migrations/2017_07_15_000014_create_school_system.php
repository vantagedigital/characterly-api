<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('city_id');
            $table->string('contact_person', 50)->index();
            $table->string('contact_no', 50)->index();
            $table->string('contact_email', 50)->index();
            $table->string('address');
            $table->string('map_location', 50)->nullable()->index();
            $table->path('profile_picture')->nullable();
            $table->path('school_logo')->nullable();
            $table->timestamps();
        });
        Schema::create('schools_curriculum', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'school_id', 'schedule_id', 'user_id');
        });
        Schema::create('schools_rewards', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'school_id', 'reward_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
        Schema::dropIfExists('schools_curriculum');
        Schema::dropIfExists('schools_rewards');
    }
}
