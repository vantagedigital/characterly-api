<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemographicsSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('provinces', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('country_id');
            $table->timestamps();
        });
        Schema::create('districts', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('province_id');
            $table->timestamps();
        });
        Schema::create('cities', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('district_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('provinces');
        Schema::dropIfExists('districts');
    }
}
