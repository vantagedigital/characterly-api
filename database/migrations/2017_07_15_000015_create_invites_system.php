<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitesSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_invites', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('teacher_id', 'user_id');
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->foreignKey('class_id');
            $table->boolean('is_accepted')->default(false);
            $table->string('auth_code', 150)->unique();
            $table->timestamps();
        });
        Schema::create('parent_invites', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('teacher_id', 'user_id');
            $table->foreignKeyAliased('parent_id', 'user_id');
            $table->boolean('is_accepted')->default(false);
            $table->string('auth_code', 150)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_invites');
        Schema::dropIfExists('class_invites');
    }
}
