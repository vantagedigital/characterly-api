<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_types', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('evaluations', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('user_id');
            $table->foreignKey('evaluation_type_id');
            $table->foreignKey('content_id');
            $table->content('content', 4000)->nullable();
            $table->unsignedInteger('points')->index();
            $table->timestamps();
        });
        Schema::create('evaluation_question_types', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('evaluation_questions', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('evaluation_id');
            $table->foreignKey('evaluation_question_type_id');
            $table->content('question', 2000);
            $table->timestamps();
        });
        Schema::create('evaluation_question_options', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('evaluation_question_id');
            $table->content('option', 2000);
            $table->boolean('is_correct');
            $table->timestamps();
        });

        Schema::create('student_evaluations', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->foreignKey('evaluation_id');
            $table->dateTime('started_at');
            $table->boolean('is_completed')->default(false);
            $table->unsignedInteger('points_earned')->default(0)->index();
            $table->timestamps();
        });
        Schema::create('student_answers', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('student_evaluation_id');
            $table->foreignKey('evaluation_question_id');
            $table->content('answer', 10000)->nullable();
            $table->boolean('is_correct');
            $table->timestamps();
        });
        Schema::create('student_answers_options', function (Blueprint $blueprint) {

            TableBuilder::createSimpleLinkTable($blueprint,
                'student_answer_id', 'evaluation_question_option_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
        Schema::dropIfExists('evaluation_questions');
        Schema::dropIfExists('evaluation_question_options');
        Schema::dropIfExists('evaluation_question_types');
        Schema::dropIfExists('evaluation_types');
        Schema::dropIfExists('evaluation_question_answers');
        Schema::dropIfExists('student_evaluation_answers_options');
    }
}
