<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classrooms', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('teacher_id', 'user_id');
            $table->foreignKey('school_id');
            $table->foreignKey('grade_id');
            $table->path('logo')->nullable();
            $table->string('motto')->nullable();
            $table->timestamps();
        });
        Schema::create('classroom_announcements', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint, 150);
            $table->foreignKey('classroom_id');
            $table->content()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classrooms');
        Schema::dropIfExists('classroom_announcements');
    }
}
