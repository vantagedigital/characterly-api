<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeLineSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_line_contents', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->polymorphicKey('profile');
            $table->foreignKey('approved_by_id');
            $table->polymorphicKey('content');
            $table->boolean('is_approved');
            $table->timestamps();
        });

        Schema::create('time_line_audios', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->path();
            $table->content('content', 2000);
            $table->boolean('is_linked');
            $table->timestamps();
        });
        Schema::create('time_line_expressions', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->string('code', 50)->index();
            $table->content('content', 2000);
            $table->timestamps();
        });
        Schema::create('time_line_links', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->path();
            $table->content('content', 2000);
            $table->timestamps();
        });
        Schema::create('time_line_notes', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->content('content', 2000);
            $table->timestamps();
        });
        Schema::create('time_line_pictures', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->path();
            $table->content('content', 2000);
            $table->boolean('is_linked');
            $table->timestamps();
        });
        Schema::create('time_line_videos', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->path();
            $table->content('content', 2000);
            $table->boolean('is_linked');
            $table->timestamps();
        });

        Schema::create('time_line_comments', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('user_id');
            $table->foreignKey('time_line_content_id');
            $table->foreignKey('parent_comment_id')->nullable();
            $table->content('content', 2000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_time_line_contents');
        Schema::dropIfExists('user_time_line_contents');
        Schema::dropIfExists('time_line_audios');
        Schema::dropIfExists('time_line_expressions');
        Schema::dropIfExists('time_line_links');
        Schema::dropIfExists('time_line_notes');
        Schema::dropIfExists('time_line_pictures');
        Schema::dropIfExists('time_line_videos');
        Schema::dropIfExists('time_line_comments');
    }
}
