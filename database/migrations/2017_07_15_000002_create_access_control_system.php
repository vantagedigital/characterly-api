<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessControlSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acl_resources', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('acl_permissions', function (Blueprint $blueprint) {
            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('user_role_id');
            $table->foreignKey('acl_resource_id');
            $table->foreignKey('applied_by')->nullable();
            $table->boolean('allow_create')->default(false);
            $table->boolean('allow_show')->default(false);
            $table->boolean('allow_edit')->default(false);
            $table->boolean('allow_delete')->default(false);
            $table->timestamps();
        });
        Schema::create('acl_activity_logs', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('user_id');
            $table->foreignKey('acl_resource_id');
            $table->foreignKey('action');
            $table->mediumText('dump')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acl_resources');
        Schema::dropIfExists('acl_permissions');
        Schema::dropIfExists('acl_activity_logs');
    }
}
