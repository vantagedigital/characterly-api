<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint, 150);
            $table->foreignKey('user_id');
            $table->foreignKey('language_id');
            $table->polymorphicKey('region');
            $table->polymorphicKey('relatable');
            $table->timestamps();
        });
        Schema::create('contents_grades', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'content_id', 'grade_id');
        });
        Schema::create('contents_pillars', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'content_id', 'pillar_id');
        });
        Schema::create('contents_tags', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'content_id', 'tag_id');
        });
        Schema::create('contents_districts', function (Blueprint $blueprint) {
            TableBuilder::createSimpleLinkTable($blueprint, 'district_id', 'content_id');
        });

        Schema::create('content_audios', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->path();
            $table->boolean('is_linked');
            $table->timestamps();
        });
        Schema::create('content_articles', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->content('content', 64000);
            $table->boolean('is_linked');
            $table->timestamps();
        });
        Schema::create('content_flash_cards', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->unsignedTinyInteger('index')->index();
            $table->content('front', 2000);
            $table->content('back', 2000);
            $table->timestamps();
        });
        Schema::create('content_images', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->path();
            $table->boolean('is_linked');
            $table->timestamps();
        });
        Schema::create('content_quotes', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('content_image_id')->nullable();
            $table->content('content', 2000);
            $table->timestamps();
        });
        Schema::create('content_videos', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->path();
            $table->boolean('is_linked');
            $table->timestamps();
        });

        Schema::create('content_approvals', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('content_id');
            $table->foreignKeyAliased('content_manager_id', 'user_id')->nullable();
            $table->foreignKeyAliased('district_manager_id', 'user_id')->nullable();
            $table->boolean('is_cm_approved');
            $table->boolean('is_dm_approved');
            $table->content('rejection_reason', 1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_types');
        Schema::dropIfExists('contents');
        Schema::dropIfExists('contents_grades');
        Schema::dropIfExists('contents_pillars');
        Schema::dropIfExists('contents_tags');
        Schema::dropIfExists('contents_districts');
        Schema::dropIfExists('content_audios');
        Schema::dropIfExists('content_articles');
        Schema::dropIfExists('content_flash_cards');
        Schema::dropIfExists('content_images');
        Schema::dropIfExists('content_quotes');
        Schema::dropIfExists('content_videos');
        Schema::dropIfExists('content_approvals');
    }
}
