<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulingSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $blueprint) {

            $table = TableBuilder::createNamedEntityTable($blueprint);
            $table->foreignKey('user_id');
            $table->content('description', 4000);
            $table->timestamps();
        });
        Schema::create('schedule_contents', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKey('schedule_id');
            $table->foreignKey('content_id');
            $table->unsignedInteger('index')->index();
            $table->dateTime('deliver_on');
            $table->boolean('was_delivered')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
        Schema::dropIfExists('schedule_contents');
    }
}
