<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $blueprint) {

            $table = TableBuilder::createAuthUsernameTable($blueprint);
            $table->foreignKey('user_role_id');
            $table->foreignKey('user_status_id');
            $table->foreignKey('school_id')->nullable();
            $table->foreignKey('classroom_id')->nullable();
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->email();
            $table->birthday()->nullable();
            $table->path('profile_picture')->nullable();
            $table->timestamps();
        });
        Schema::create('user_roles', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('user_statuses', function (Blueprint $blueprint) {
            TableBuilder::createSimpleNamedEntityTable($blueprint);
        });
        Schema::create('parents_students', function (Blueprint $blueprint) {

            $table = TableBuilder::createLinkTable($blueprint);
            $table->foreignKey('user_id');
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_roles');
        Schema::dropIfExists('parents_students');
    }
}
