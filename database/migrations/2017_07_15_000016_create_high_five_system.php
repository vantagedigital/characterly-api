<?php

use APIInterface\Server\TableBuilder;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighFiveSystem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('high_fives', function (Blueprint $blueprint) {

            $table = TableBuilder::createEntityTable($blueprint);
            $table->foreignKeyAliased('student_id', 'user_id');
            $table->foreignKey('evaluation_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('high_fives');
    }
}
