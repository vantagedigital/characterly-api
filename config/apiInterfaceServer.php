<?php

return array(

    /*
     * Note: Application namespace is detected automatically.
     */

    /*
     * Default Security Provider
     */
    'authentication' => array(

        'default' => 'jwt',
        'methods' => array(

            'jwt' => array(

                'model' => 'User',
                /*
                 * This setting controls the registration of internal middleware. To disable, replace with 'custom'
                 */
                'middleware' => 'internal',

            ),

        ),

    ),

    /*
     * Information to include in generated controllers
     */
    'controllers' => array(

        'path' => 'app\\Http\\Controllers\\',
        'namespace' => 'Controllers',
        'baseClass' => \Characterly\Controllers\Controller::class,

    ),

    /*
     * Information regarding model structure
     */
    'models' => array(

        'path' => 'app\\Models',
        'namespace' => 'Models',

    ),

    /*
     * Information to include in generated route files
     */
    'routes' => array(

        /*
         * Possible types: laravel, dingo
         */
        'router' => 'dingo',
        'path' => 'routes\\',

        /*
         * To use the internal general purpose routes, use 'internal', otherwise specify the path from project root to
         * the templates, you have made.
         */
        'templates' => 'routes\\templates\\',

    ),

    'errors' => array(

        /*
         * To specify your own, just replace with the namespaced class of your handler. Extend the class with the
         * internal error handler.
         */
        'handler' => 'internal'

    ),

);