<?php

namespace Characterly\Repositories;

use APIInterface\Server\ResponseGeneratorsTrait;

class BaseRepository
{
    use ResponseGeneratorsTrait;

    /**
     * @return \Characterly\Models\User
     */
    public static function getCurrentUser()
    {
        /** @var \Tymon\JWTAuth\JWTAuth $parsedToken */
        $parsedToken = \JWTAuth::parseToken();
        return \JWTAuth::toUser($parsedToken);
    }

    public static function generateError($error, $parameters = array())
    {
        if (count($parameters)) {
            $error['parameters'] = $parameters;
        }
        $instance = new static();
        $instance->generateErrorResponse($error);
    }
}