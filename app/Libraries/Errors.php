<?php

namespace Characterly\Libraries;

class Errors
{
    public static $errors = array(
        'ACL-InvalidRole' => array('code' => 'ACL-001', 'message' => 'Specified role is invalid'),
    );
}