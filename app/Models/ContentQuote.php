<?php

namespace Characterly\Models;

/**
 * Characterly\Models\ContentQuote
 *
 * @property int $id
 * @property int|null $content_image_id
 * @property \Characterly\Models\Content $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\ContentImage|null $contentImage
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentQuote whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentQuote whereContentImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentQuote whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentQuote whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentQuote whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContentQuote extends BaseModel
{
    public function content()
    {
        return $this->belongsTo(Content::class);
    }

    public function contentImage()
    {
        return $this->belongsTo(ContentImage::class);
    }
}