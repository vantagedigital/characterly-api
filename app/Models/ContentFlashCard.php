<?php

namespace Characterly\Models;

/**
 * Characterly\Models\ContentFlashCard
 *
 * @property int $id
 * @property int $index
 * @property string $front
 * @property string $back
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Content $content
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereFront($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentFlashCard whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContentFlashCard extends BaseModel
{
    public function content()
    {
        return $this->belongsTo(Content::class);
    }
}