<?php

namespace Characterly\Models;

/**
 * Characterly\Models\EvaluationQuestionOption
 *
 * @property int $id
 * @property int $evaluation_question_id
 * @property string $option
 * @property int $is_correct
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\EvaluationQuestion $evaluationQuestion
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereEvaluationQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereOption($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestionOption whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EvaluationQuestionOption extends BaseModel
{
    public function evaluationQuestion()
    {
        return $this->belongsTo(EvaluationQuestion::class);
    }
}