<?php

namespace Characterly\Models;

/**
 * Characterly\Models\StudentAnswer
 *
 * @property int $id
 * @property int $student_evaluation_id
 * @property int $evaluation_question_id
 * @property string|null $answer
 * @property int $is_correct
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\EvaluationQuestion $evaluationQuestion
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\EvaluationQuestionOption[] $studentAnswerOptions
 * @property-read \Characterly\Models\StudentEvaluation $studentEvaluation
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereEvaluationQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereStudentEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentAnswer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudentAnswer extends BaseModel
{
    public function evaluationQuestion()
    {
        return $this->belongsTo(EvaluationQuestion::class);
    }

    public function studentAnswerOptions()
    {
        return $this->belongsToMany(EvaluationQuestionOption::class, 'student_answers_options')
            ->withTimestamps();
    }

    public function studentEvaluation()
    {
        return $this->belongsTo(StudentEvaluation::class);
    }
}