<?php

namespace Characterly\Models;

/**
 * Characterly\Models\AclPermission
 *
 * @property int $id
 * @property string $name
 * @property int $user_role_id
 * @property int $acl_resource_id
 * @property int|null $applied_by
 * @property int $allow_create
 * @property int $allow_show
 * @property int $allow_edit
 * @property int $allow_delete
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\AclResource $aclResource
 * @property-read \Characterly\Models\UserRole $userRole
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAclResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAllowCreate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAllowDelete($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAllowEdit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAllowShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereAppliedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclPermission whereUserRoleId($value)
 * @mixin \Eloquent
 */
class AclPermission extends BaseModel
{
    public function aclResource()
    {
        return $this->belongsTo(AclResource::class);
    }

    public function userRole()
    {
        return $this->belongsTo(UserRole::class);
    }
}