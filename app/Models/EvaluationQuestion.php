<?php

namespace Characterly\Models;

/**
 * Characterly\Models\EvaluationQuestion
 *
 * @property int $id
 * @property int $evaluation_id
 * @property int $evaluation_question_type_id
 * @property string $question
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Evaluation $evaluation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\EvaluationQuestionOption[] $evaluationQuestionOptions
 * @property-read \Characterly\Models\EvaluationQuestionType $evaluationQuestionType
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereEvaluationQuestionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\EvaluationQuestion whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EvaluationQuestion extends BaseModel
{
    public function evaluation()
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function evaluationQuestionType()
    {
        return $this->belongsTo(EvaluationQuestionType::class);
    }

    public function evaluationQuestionOptions()
    {
        return $this->hasMany(EvaluationQuestionOption::class);
    }
}