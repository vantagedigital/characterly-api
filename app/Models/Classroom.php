<?php

namespace Characterly\Models;

/**
 * Characterly\Models\Classroom
 *
 * @property int $id
 * @property int $teacher_id |AO:user_id
 * @property int $school_id
 * @property int $grade_id
 * @property string|null $logo
 * @property string|null $motto
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ClassroomAnnouncement[] $classroomAnnouncements
 * @property-read \Characterly\Models\Grade $grade
 * @property-read \Characterly\Models\School $school
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\User[] $students
 * @property-read \Characterly\Models\User $teacher
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereGradeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereMotto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereTeacherId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Classroom whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Classroom extends BaseModel
{
    public function classroomAnnouncements()
    {
        return $this->hasMany(ClassroomAnnouncement::class);
    }

    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function students()
    {
        return $this->hasMany(User::class);
    }

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }
}