<?php

namespace Characterly\Models;

/**
 * Characterly\Models\Content
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $language_id
 * @property string $region_type
 * @property int $region_id R:OM
 * @property string $relatable_type
 * @property int $relatable_id R:OM
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\ContentApproval $contentApproval
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentArticle[] $contentArticles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentAudio[] $contentAudios
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentFlashCard[] $contentFlashCards
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentImage[] $contentImages
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentQuote[] $contentQuotes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentVideo[] $contentVideos
 * @property-read \Characterly\Models\User $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\District[] $districts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Grade[] $grades
 * @property-read \Characterly\Models\Language $language
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Pillar[] $pillars
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $region
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $relatable
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereRegionType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereRelatableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereRelatableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Content whereUserId($value)
 * @mixin \Eloquent
 */
class Content extends BaseModel
{
    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function contentApproval()
    {
        return $this->hasOne(ContentApproval::class);
    }

    public function contentArticles()
    {
        return $this->hasMany(ContentArticle::class);
    }

    public function contentAudios()
    {
        return $this->hasMany(ContentAudio::class);
    }

    public function contentFlashCards()
    {
        return $this->hasMany(ContentFlashCard::class);
    }

    public function contentImages()
    {
        return $this->hasMany(ContentImage::class);
    }

    public function contentQuotes()
    {
        return $this->hasMany(ContentQuote::class);
    }

    public function contentVideos()
    {
        return $this->hasMany(ContentVideo::class);
    }

    public function districts()
    {
        return $this->belongsToMany(District::class);
    }

    public function grades()
    {
        return $this->belongsToMany(Grade::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function pillars()
    {
        return $this->belongsToMany(Pillar::class);
    }

    public function region()
    {
        return $this->morphTo();
    }

    public function relatable()
    {
        return $this->morphTo();
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }
}