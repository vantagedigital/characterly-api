<?php

namespace Characterly\Models;

/**
 * Characterly\Models\Evaluation
 *
 * @property int $id
 * @property int $user_id
 * @property int $evaluation_type_id
 * @property int $content_id
 * @property \Characterly\Models\Content $content
 * @property int $points
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\User $creator
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\EvaluationQuestion[] $evaluationQuestions
 * @property-read \Characterly\Models\EvaluationType $evaluationType
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\StudentEvaluation[] $studentEvaluations
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereEvaluationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\Evaluation whereUserId($value)
 * @mixin \Eloquent
 */
class Evaluation extends BaseModel
{
    public function content()
    {
        return $this->belongsTo(Content::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function evaluationQuestions()
    {
        return $this->hasMany(EvaluationQuestion::class);
    }

    public function evaluationType()
    {
        return $this->belongsTo(EvaluationType::class);
    }

    public function studentEvaluations()
    {
        return $this->hasMany(StudentEvaluation::class);
    }
}