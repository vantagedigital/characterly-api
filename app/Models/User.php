<?php

namespace Characterly\Models;

/**
 * Characterly\Models\User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string|null $remember_token
 * @property int $user_role_id
 * @property int $user_status_id
 * @property int|null $school_id
 * @property int|null $classroom_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string|null $birth_date
 * @property string|null $profile_picture
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Classroom|null $classroom
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentApproval[] $contentManagerApprovals
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Content[] $contents
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Classroom[] $createdClassrooms
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\Evaluation[] $createdEvaluations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\ContentApproval[] $districtManagerApprovals
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\User[] $parents
 * @property-read \Characterly\Models\School|null $school
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\StudentEvaluation[] $studentEvaluations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\User[] $students
 * @property-read \Characterly\Models\UserRole $userRoles
 * @property-read \Characterly\Models\UserStatus $userStatus
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereProfilePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereSchoolId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereUserRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereUserStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends BaseModel
{
    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    public function createdClassrooms()
    {
        return $this->hasMany(Classroom::class, 'teacher_id', 'user_id');
    }

    public function createdEvaluations()
    {
        return $this->hasMany(Evaluation::class);
    }

    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public function contentManagerApprovals()
    {
        return $this->hasMany(ContentApproval::class, 'content_manager_id', 'user_id');
    }

    public function districtManagerApprovals()
    {
        return $this->hasMany(ContentApproval::class, 'district_manager_id', 'user_id');
    }

    public function parents()
    {
        return $this->belongsToMany(User::class, 'parents_students', 'student_id', 'user_id')
            ->withTimestamps();
    }

    public function school()
    {
        return $this->belongsTo(School::class);
    }

    public function studentEvaluations()
    {
        return $this->hasMany(StudentEvaluation::class, 'student_id', 'user_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, 'parents_students', 'user_id', 'student_id')
            ->withTimestamps();
    }

    public function userRoles()
    {
        return $this->belongsTo(UserRole::class);
    }

    public function userStatus()
    {
        return $this->belongsTo(UserStatus::class);
    }
}