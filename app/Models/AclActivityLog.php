<?php

namespace Characterly\Models;

/**
 * Characterly\Models\AclActivityLog
 *
 * @property int $id
 * @property int $user_id
 * @property int $acl_resource_id
 * @property int $action
 * @property string|null $dump
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\AclResource $aclResource
 * @property-read \Characterly\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereAclResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereDump($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclActivityLog whereUserId($value)
 * @mixin \Eloquent
 */
class AclActivityLog extends BaseModel
{
    public function aclResource()
    {
        return $this->belongsTo(AclResource::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}