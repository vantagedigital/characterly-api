<?php

namespace Characterly\Models;

/**
 * Characterly\Models\AclResource
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\AclActivityLog[] $aclActivityLogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\AclPermission[] $aclPermissions
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclResource whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclResource whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclResource whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\AclResource whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AclResource extends BaseModel
{
    public function aclActivityLogs()
    {
        return $this->hasMany(AclActivityLog::class);
    }

    public function aclPermissions()
    {
        return $this->hasMany(AclPermission::class);
    }
}