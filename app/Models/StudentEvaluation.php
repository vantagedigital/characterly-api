<?php

namespace Characterly\Models;

/**
 * Characterly\Models\StudentEvaluation
 *
 * @property int $id
 * @property int $student_id |AO:user_id
 * @property int $evaluation_id
 * @property string $started_at
 * @property int $is_completed
 * @property int $points_earned
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Evaluation $evaluation
 * @property-read \Illuminate\Database\Eloquent\Collection|\Characterly\Models\StudentAnswer[] $studentAnswers
 * @property-read \Characterly\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereEvaluationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereIsCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation wherePointsEarned($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereStudentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\StudentEvaluation whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StudentEvaluation extends BaseModel
{
    public function evaluation()
    {
        return $this->belongsTo(Evaluation::class);
    }

    public function studentAnswers()
    {
        return $this->hasMany(StudentAnswer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'student_id');
    }
}