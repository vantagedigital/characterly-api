<?php

namespace Characterly\Models;

/**
 * Characterly\Models\ContentApproval
 *
 * @property int $id
 * @property int $content_id
 * @property int|null $content_manager_id |AO:user_id
 * @property int|null $district_manager_id |AO:user_id
 * @property int $is_cm_approved
 * @property int $is_dm_approved
 * @property string $rejection_reason
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Content $content
 * @property-read \Characterly\Models\User|null $contentManager
 * @property-read \Characterly\Models\User|null $districtManager
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereContentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereContentManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereDistrictManagerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereIsCmApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereIsDmApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereRejectionReason($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentApproval whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContentApproval extends BaseModel
{
    public function content()
    {
        return $this->belongsTo(Content::class);
    }

    public function contentManager()
    {
        return $this->belongsTo(User::class, 'content_manager_id');
    }

    public function districtManager()
    {
        return $this->belongsTo(User::class, 'district_manager_id');
    }
}