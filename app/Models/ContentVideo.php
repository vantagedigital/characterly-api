<?php

namespace Characterly\Models;

/**
 * Characterly\Models\ContentVideo
 *
 * @property int $id
 * @property string $path
 * @property int $is_linked
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Content $content
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentVideo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentVideo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentVideo whereIsLinked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentVideo wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ContentVideo whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContentVideo extends BaseModel
{
    public function content()
    {
        return $this->belongsTo(Content::class);
    }
}