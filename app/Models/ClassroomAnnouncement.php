<?php

namespace Characterly\Models;

/**
 * Characterly\Models\ClassroomAnnouncement
 *
 * @property int $id
 * @property string $name
 * @property int $classroom_id
 * @property string|null $content
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\Classroom $classroom
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereClassroomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\ClassroomAnnouncement whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ClassroomAnnouncement extends BaseModel
{
    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }
}