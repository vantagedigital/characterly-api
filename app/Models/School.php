<?php

namespace Characterly\Models;

/**
 * Characterly\Models\School
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 * @property string $contact_person
 * @property string $contact_no
 * @property string $contact_email
 * @property string $address
 * @property string|null $map_location
 * @property string|null $profile_picture
 * @property string|null $school_logo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Characterly\Models\City $city
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel like($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel relation($relation)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel toSubQuery($key, $returnExpression = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel unlike($column, $value, $before = '%', $after = '%', $andWhere = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereContactEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereContactNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereContactPerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereMapLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereNotInSubQuery($column, $subQuery, $subQueryColumn)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereProfilePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\BaseModel whereRelatable($type, $id = 0, $name = 'relatable')
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereSchoolLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Characterly\Models\School whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class School extends BaseModel
{
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}