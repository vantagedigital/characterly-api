<?php

namespace Characterly\Controllers;

use APIInterface\Server\errors\ValidationErrorsTrait;
use APIInterface\Server\ResponseGeneratorsTrait;

class Controller extends \Illuminate\Routing\Controller
{
    use ResponseGeneratorsTrait, ValidationErrorsTrait;
}