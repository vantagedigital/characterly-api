<?php

namespace Characterly\Controllers;

use APIInterface\Server\Controllers\OneEntityTrait;
use Characterly\Models\User;

class UserController extends Controller
{
    use OneEntityTrait;

    /**
     * Model class of the entity
     *
     * @var string
     */
    public $firstModel = User::class;
}