<?php

namespace Characterly\Controllers;

use APIInterface\Server\Controllers\OneEntityTrait;
use Characterly\Models\AclActivityLog;

class AclActivityLogController extends Controller
{
    use OneEntityTrait;

    /**
     * Model class of the entity
     *
     * @var string
     */
    public $firstModel = AclActivityLog::class;
}