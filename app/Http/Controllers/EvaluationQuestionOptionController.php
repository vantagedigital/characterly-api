<?php

namespace Characterly\Controllers;

use Characterly\Controllers\Controller;
use APIInterface\Server\Controllers\OneEntityTrait;
use Characterly\Models\EvaluationQuestionOption;

class EvaluationQuestionOptionController extends Controller
{
    use OneEntityTrait;

    /**
     * Model class of the entity
     *
     * @var string
     */
    public $firstModel = EvaluationQuestionOption::class;
}