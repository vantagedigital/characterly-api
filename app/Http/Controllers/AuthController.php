<?php

namespace Characterly\Controllers;

use APIInterface\Server\Controllers\JWTAuthTrait;

class AuthController extends Controller
{
    use JWTAuthTrait;

    /**
     * @var array
     */
    public $authUserEagerLoad = array('aclRole');
}