<?php

namespace Characterly\Controllers;

use Characterly\Controllers\Controller;
use APIInterface\Server\Controllers\OneEntityTrait;
use Characterly\Models\Tag;

class TagController extends Controller
{
    use OneEntityTrait;

    /**
     * Model class of the entity
     *
     * @var string
     */
    public $firstModel = Tag::class;
}