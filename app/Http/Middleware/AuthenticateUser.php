<?php

namespace Characterly\Middleware;

use APIInterface\Server\JWT\BaseRequestMiddleware;
use Characterly\Models\User;

class AuthenticateUser extends BaseRequestMiddleware
{
    /**
     * @var string
     */
    public $authModel = User::class;
}