<?php

namespace Characterly\Http\Middleware;

use APIInterface\Server\JWT\AuthMiddleware;
use Characterly\Models\User;

class RefreshToken extends AuthMiddleware
{
    /**
     * @var string
     */
    public $authModel = User::class;
}